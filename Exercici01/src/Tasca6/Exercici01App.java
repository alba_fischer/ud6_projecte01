package Tasca6;

import javax.swing.JOptionPane;
public class Exercici01App {
	public static void main(String[] args) {
		/* Creem una aplicaci� que ens calculi l'�rea d'un cercle, quadrat o triangle.
		 * Demanar� la figura de la qual volem calcular l'�rea i els valors necessaris per fer-ho. 
		 */
		
		String figura = JOptionPane.showInputDialog("Introdueix la figura en min�scula");
		
		
	
		if (figura.equals( "cercle")) {
			double area = cercle();
			System.out.println("L'�rea del cercle �s: "+ area);
		}else if (figura.equals("triangle")) {
			double area = triangle();
			System.out.println("L'�rea del triangle �s: "+ area);
		}else if (figura.equals("quadrat")){
			double area = quadrat();
			System.out.println("L'�rea del quadrat �s: " + area);
		}
		
	}
		
	public static double cercle() {
		// Calcula l'�rea d'un cercle
		
		String radi_text = JOptionPane.showInputDialog("Introdueix el valor del radi");
		double radi = Double.parseDouble(radi_text);
		
		double area;
		area  = Math.pow(radi, 2) * Math.PI;
			
		return area;
	}
	public static double triangle() {
		// Calcula l'�rea d'un triangle
		
		String base_text = JOptionPane.showInputDialog("Introdueix el valor de la base");
		double base = Double.parseDouble(base_text);
		String altura_text = JOptionPane.showInputDialog("Introdueix el valor de l'altura");
		double altura= Double.parseDouble(altura_text);
		double area;
		area  = (base*altura)/2;
			
		return area;
	}	
	public static double quadrat() {
		// Calcula l'�rea d'un quadrat
		
		String costat_text = JOptionPane.showInputDialog("Introdueix el valor del costat");
		double costat = Double.parseDouble(costat_text);

		double area;
		area  = costat*costat;
			
		return area;
	}	
			
	
	
}