package Tasca6;

import java.util.Scanner;
public class Exercici02App {

	public static void main(String[] args) {
		/* Creem una aplicaci� que ens genera nombres aleat�ris
		 * 
		 */
		
		Scanner scan = new Scanner (System.in);
		System.out.println("Introdueix la quantitat de nombres a generar:");
		int numQuant = scan.nextInt();
		System.out.println("Introdueix el l�mit inferior de l'interval:");
		int numMin = scan.nextInt();
		System.out.println("Introdueix el l�mit superior de l'interval:");
		int numMax = scan.nextInt();
		
		System.out.println("Els nombres generats s�n:");
		int i;
		for (i = 0; i < numQuant; i++) {
			System.out.println(numAleatori (numMin, numMax));
		}
		scan.close();
	}
	
	public static int numAleatori (int numMin, int numMax)	{
		//Generem nombres enters aleat�ris entre l'interval introdu�t per teclat
		int numAle = (int) (Math.random()*(numMax-numMin) + numMin);
		
		return numAle;
	}
		
	

}
