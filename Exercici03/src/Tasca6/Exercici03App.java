package Tasca6;

import java.util.Scanner;

public class Exercici03App {

	public static void main(String[] args) {
		/* Creem una aplicaci� que ens demani un nombre per teclat
		 * i ens digui si �s un nombre primer o no.
		 */
		Scanner scan = new Scanner (System.in);
		System.out.println("Introdueix un nombre:");
		int num = scan.nextInt();
		
		if (numPrimer (num)== true) {
			System.out.println(num + " �s un nombre primer");
		}else {
			System.out.println(num + " no �s un nombre primer");
		scan.close();
		}
	}
	
	public static boolean numPrimer (int numero) {
		// Determinem si el nombre �s primer o no
		
		int i;
		boolean primer = true;
		for (i = 2; i < numero; i++) {
			if (numero % i == 0) {
				primer = false;
			}						
		}
		return primer;
	}

}
