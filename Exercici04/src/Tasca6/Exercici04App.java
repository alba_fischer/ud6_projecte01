package Tasca6;

import java.util.Scanner;

public class Exercici04App {

	public static void main(String[] args) {
		// Creem una aplicaci� per calcular el factorial d'un nombre.
		Scanner scan = new Scanner (System.in);
		System.out.println("Introdueix un nombre:");
		int num = scan.nextInt();
		System.out.println("El factorial de " + num + " �s: "  + factorial (num));
		scan.close();
	}
	public static int factorial (int numero) {
		//Calculem el factorial del nombre
		
		int i, fact = 1;
		for (i = 1; i <= numero; i++) {
			fact = 	fact * i;		
		}
		return fact;
	}
}
