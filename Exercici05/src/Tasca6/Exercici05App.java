package Tasca6;

import java.util.Scanner;

public class Exercici05App {
	public static void main(String[] args) {
		// Creem una aplicaci� que ens converteix un nombre en base decimal a bin�ria.
	
		Scanner scan = new Scanner (System.in);
		System.out.println("Introdueix un nombre:");
		int num = scan.nextInt();
		System.out.println( binari(num));
		scan.close();
	}
	public static String binari(int numero) {
		//Passem de sistema decimal a bin�ri
		int bin  = 0, pot = 0;
		while(numero != 0) {
			int div = numero % 2;
			bin = bin + div * (int) Math.pow(10, pot);
			pot++;
			numero = numero/2;			
		}
		
		String numBinari = String.valueOf(bin);
		
		return numBinari;
		// Tamb� ho podriem fer amb la funci� Integer.toBinaryString(numero)	
	}
}