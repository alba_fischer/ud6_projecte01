package Tasca6;

import java.util.Scanner;

public class Exercici06App {

	public static void main(String[] args) {
		// Creem una aplicaci� que ens conta el nombre de xifres d'un n�mero.
		
		Scanner scan = new Scanner (System.in);
		System.out.println("Introdueix un nombre enter i positiu:");
		int num = scan.nextInt();
		if (num < 0) {
			System.out.println("T'has equivocat, el nombre havia de ser positiu!");
		}else {
		System.out.println("El nombre de xifres de " + num +" �s " + nombreXifres(num));
		}
		scan.close();
	}
	public static int nombreXifres (int numero) {
		//Calculem el nombre de xifres del nombre introdu�t per teclat
		String xifres = Integer.toString(numero);
		int numXifres = xifres.length();
		return numXifres;
	}

}
