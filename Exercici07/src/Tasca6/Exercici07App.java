package Tasca6;

import java.util.Scanner;

public class Exercici07App {

	public static void main(String[] args) {
		/* Creem una aplicació que ens converteixi una quantitat
		 * d'euros a dolars, iens o lliures.
		 */
		Scanner scan = new Scanner (System.in);
		System.out.println("Introdueix la quantitat d'euros:");
		int num = scan.nextInt();
		System.out.println("Introdueix el tipus de moneda a la que vols convertir els euros:");
		String moneda = scan.next();
		canviMoneda(num, moneda);
		scan.close();
	}	
	public static void canviMoneda(int euros, String moneda) {
		// Convertim els euros a la moneda escollida
		
		double diners;
		
		switch (moneda) {
			case "lliures":
				diners = euros*0.86;
				System.out.println("Els " + euros + " € equivalent a " + diners + " lliures.");
				break;
			case "iens":
				diners = euros*129.852;
				System.out.println("Els " + euros + " € equivalent a " + diners + " iens.");
				break;
			case "dolars":
				diners = euros*1.28611;
				System.out.println("Els " + euros + " € equivalent a " + diners + " dolars.");
				break;
			default:
				System.out.println("Aquesta moneda no l'hem tingut en compte en aquesta aplicació");
				break;
			
		}

	}
}
