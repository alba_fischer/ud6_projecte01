package Tasca6;

import java.util.Scanner;

public class Exercici08App {

	public static void main(String[] args) {
		//Creem una array de 10 posicions de nombres demanats per teclat.		
		int num[] = emplenarArray();
		mostrarArray(num);
				
	}
	public static int[] emplenarArray() {
		//Creem l'array en valors demanats per teclat
		int num[] = new int[10];
		Scanner scan = new Scanner (System.in);
		for (int i = 0;i < num.length;i++) {
			System.out.println("Introdueix un nombre que contindr� l'array:");
			num[i] = scan.nextInt();
		}
		scan.close();
		return num;
	}
	public static void mostrarArray(int numero[]){
		//Mostrem els �ndex i els valors per pantalla
		for (int i = 0;i < numero.length;i++) {
			System.out.println("�ndex "+i+ " = " + numero[i]);
		}
		
	}
}
