package Tasca6;

import java.util.Scanner;

public class Exercici09App {

	public static void main(String[] args) {
		// Generem un array de nombres aleat�ris
		int num[] = emplenarArray(0,9);
		mostrarArray(num);
	}
	public static int[] emplenarArray(int numMin, int numMax) {
		//Creem l'array en valors aleat�ris
		
		Scanner scan = new Scanner (System.in);
		System.out.println("Introdueix la dimensi� de l'array:");
		int dimensio = scan.nextInt();
		int num[] = new int[dimensio];
		
		for (int i = 0;i < num.length;i++) {
			num[i] = (int) (Math.random()*(numMax-numMin) + numMin); //els nombres aleat�ris que generem s�n enters
		}
		scan.close();
		return num;
	}
	public static void mostrarArray(int numero[]){
		//Mostrem el valor de cada posici� i la suma de totes per pantalla
		int suma = 0;
		for (int i = 0;i < numero.length;i++) {
			System.out.println("�ndex "+i+ " = " + numero[i]);
			suma  = suma + numero[i];
		}
		System.out.println("La suma dels diferents valors que cont� l'array �s:"+suma);
		
	}
}
