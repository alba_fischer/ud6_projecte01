package Tasca6;

import java.util.Scanner;

public class Exercici10App {

	public static void main(String[] args) {
		// Creem un array de nombres primers aleat�ris.
		Scanner scan = new Scanner (System.in);
		System.out.println("Introdueix el l�mit inferior dels nombres aleat�ris:");
		int min = scan.nextInt();
		System.out.println("Introdueix el l�mit superior dels nombres aleat�ris:");
		int max = scan.nextInt();
		System.out.println("Introdueix la dimensi� de l'array:");
		int dimensio = scan.nextInt();
		scan.close();
		int num[] = emplenarArray(min,max, dimensio);
		mostrarArray(num);
	}	
	
	public static boolean numPrimer (int numero) {
		// Determinem/comprovem si el nombre �s primer o no
		
		int i;
		boolean primer = true;
		for (i = 2; i < numero; i++) {
			if (numero % i == 0) {
				primer = false;	
			} 						
		}
		return primer;
	}
	public static int[] emplenarArray(int numMin, int numMax, int dimensio) {
		//Creem l'array en valors primers aleat�ris 
		
		int num[] = new int[dimensio];
				
		for (int i = 0;i < num.length;i++) {
			do {
				num[i] = (int) (Math.random()*(numMax-numMin) + numMin); //els nombres aleat�ris que generem s�n enters	
			}while(numPrimer(num[i]) == false);					
		}
		
		return num;
	}
	public static void mostrarArray(int numero[]){
		//Mostrem el valor de cada posici� i tamb� el valor m�s gran que cont� l'array.
		int maxim = numero[0];
		for (int i = 0;i < numero.length;i++) {
			System.out.println("�ndex "+i+ " = " + numero[i]);	
			if (i>=1) {
				if (numero[i]>maxim) {
					maxim = numero[i];
				}
			}
		}

		System.out.println("El nombre m�s gran que cont� l'array �s:" + maxim);
		
	}
}
