package Tasca6;

import java.util.Scanner;
import java.util.Random;

public class Exercici11App {

	public static void main(String[] args) {
		/* Creem un array de nombres aleat�ris, la copiem a una altra array
		 * sobreescrivim els nombres de la primera array, i multipliquem 
		 * el nombre de l'array 1 de la posici� 1 pel de la posici� 1 de l'array 2. I aix�
		 * en totes les posicions. Ho guardem a una tercera array i mostrem el contingut d'aquesta
		 * per pantalla.
		 */
		Scanner scan = new Scanner (System.in);
		System.out.println("Introdueix la dimensi� del array:");
		int dimensio = scan.nextInt();
		int array1[] = new int[dimensio];
		int array2[] = new int[dimensio];
		scan.close();
		Random random = new Random();
		System.out.print("Array1 = ");
		for (int i = 0;i < dimensio;i++) {
			array1[i] = random.nextInt(200);//els nombres aleat�ris que generem s�n enters						
			// interval nombres aleat�ris [0,200)
			array2[i] = array1[i];
			array1[i] = random.nextInt(200); 
			System.out.print(array1 [i]+ " ");
		}
		System.out.println();
		System.out.print("Array2 = ");
		for (int i = 0;i < dimensio;i++) {
			System.out.print(array2 [i]+ " ");
		}
		System.out.println();
		multiplicarArray(array1, array2); 
		
		
	}
	public static void multiplicarArray(int num [], int num1 []) {
		//Multipliquem els valors de array1 * array2
		int array3[] = new int[num.length];
		System.out.print("Array3 = ");
		for (int i = 0;i < num.length;i++) {
			array3 [i] = num[i]*num1[i];
			System.out.print(array3 [i]+ " ");
		}
		
	}
}
