package Tasca6;

import java.util.Scanner;
import java.util.Random;

public class Exercici12 {

	public static void main(String[] args) {
		/* Generem un array de nombres aleatoris i mostrem
		 * els nombres que acabin amb el digit que introdu�m per teclat
		 */
		Scanner scan = new Scanner (System.in);
		System.out.println("Introdueix un d�git:");
		int digit = scan.nextInt();
		
		boolean control = controlArray(digit);
				
		if (control == true) {
			System.out.println("El d�git �s correcte!");
			System.out.println("Introdueix la dimensi� del array:");
			int dimensio = scan.nextInt();
			scan.close();
			int array[] = crearArray(dimensio);
			int array1[] = new int [array.length];
			System.out.print("Els nombres, la �ltima xifra dels quals concideix amb el d�git introdu�t, s�n:");
			for (int i = 0;i < array.length;i++) {
				if (digit == array[i]%10) {					
					System.out.print(array[i]+ " ");
					array1[i] = array[i];	
				}
			}
			System.out.println(" ");
			System.out.print("Nou array:");
			for (int i = 0;i < array.length;i++) {
				System.out.print(array1[i]+ " ");// hem creat un nou array de la mateixa dimensi� que el primer, 
				/*nom�s s'introdueixen a l'array els nombres seleccionats pel seu acabament. 
				 * Obviament, l'array nou no s'ompli tot, per aix� al mostrarlo per pantalla surten 0.
				 */
				
			}
		}else{
			System.out.println("El d�git �s incorrecte, torna a comen�ar!");
			}
		
	}
	public static int[] crearArray(int dimensio) {
		//Creem l'array en valors primers aleat�ris 
		
		int array[] = new int[dimensio];
		Random random = new Random();	
		System.out.print("Array que generem : ");
		for (int i = 0;i < array.length;i++) {			
				array[i] = random.nextInt(300)+1;//Genera nombres aleat�ris en l'interval [1,300]	
				System.out.print(array[i] + " ");
		}
		System.out.println();
		return array;
	}
	public static boolean controlArray(int digit) {
		//control que s'introdueixi un nombre correcte
		
		int i;
		boolean control = false;
		for (i = 0; i<=9; i++) {
			if (digit == i) {
				control = true;
			}	
		}
		return control;
	}
}